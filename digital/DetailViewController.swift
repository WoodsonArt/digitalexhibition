//
//  DetailViewController.swift
//  digital
//
//  Created by Jane Weinke on 8/18/15.
//  Copyright (c) 2015 Leigh Yawkey Woodsn Art Museum. All rights reserved.
//
import Foundation
import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UITextView!
    @IBOutlet weak var image                 : UIImageView!;

    var detailItem: Artwork? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail:Artwork = self.detailItem {
            if let label:UITextView = self.detailDescriptionLabel {
                
                var docsPath = NSBundle.mainBundle().resourcePath!
                let fileManager = NSFileManager.defaultManager()
                
                var error: NSError?
                let docsArray = fileManager.contentsOfDirectoryAtPath(docsPath, error:&error)
                
                for filename in docsArray!
                {
                    if filename.hasPrefix(String(detail.id) + " ")
                    {
                        image.image = UIImage(named: filename as! String);
                    }
                }
                
                
                self.navigationItem.title = detail.artworkTitle;
                
                var captionText:String = String();
                captionText = "";
                captionText += detail.artistFirstName;
                captionText += " ";
                captionText += detail.artistLastName;
                captionText += "   ";
                captionText += detail.artistNationality;
                captionText += "   ";
                captionText += detail.artistLifeDates;
                captionText += "\n";
                captionText += detail.artworkTitle;
                captionText += ", ";
                captionText += String(detail.artworkYear);
                captionText += "\n";
                captionText += detail.artworkSpecies
                captionText += "\n"
                captionText += detail.artworkMedium;
                captionText += " on ";
                captionText += detail.artworkSupport;
                captionText += "\n";
                captionText += detail.artworkUnframedDimensions;
                captionText += "\n"
                captionText += detail.source;
                captionText += "\n";
                captionText += detail.artworkExhibitionName;
                let nsStringCaptionText:NSString = NSString(string: captionText);
                let nsStringCaptionTextAttributed:NSMutableAttributedString = NSMutableAttributedString(string: nsStringCaptionText as String);
                let titleRange:NSRange = nsStringCaptionText.rangeOfString(detail.artworkTitle, options: NSStringCompareOptions.BackwardsSearch);
                let nameRange:NSRange = nsStringCaptionText.rangeOfString(detail.artistFirstName + " " + detail.artistLastName, options: NSStringCompareOptions.BackwardsSearch);
                var paragraphStyle = NSMutableParagraphStyle();
                paragraphStyle.lineSpacing = 2.0;
                nsStringCaptionTextAttributed.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: count(captionText)));
                nsStringCaptionTextAttributed.addAttribute(NSFontAttributeName, value: UIFont(name: "Avenir", size: 21)!, range: NSRange(location: 0, length: count(captionText)));
                nsStringCaptionTextAttributed.addAttribute(NSFontAttributeName, value: UIFont(name: "Avenir-BookOblique", size: 21)!, range: titleRange);
                nsStringCaptionTextAttributed.addAttribute(NSFontAttributeName, value: UIFont(name: "Avenir-Heavy", size: 21)!, range: nameRange);
                label.attributedText = nsStringCaptionTextAttributed;
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

