import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate
{
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        UINavigationBar.appearance().barTintColor = UIColor(red: 147.0/255.0, green: 140.0/255.0, blue: 126.0/255.0, alpha:1.0);
        UINavigationBar.appearance().tintColor = UIColor.whiteColor();
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Avenir", size: 21)!];
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Avenir", size: 21)!], forState: UIControlState.Normal);
        UILabel.appearance().font = UIFont(name: "Avenir", size: 17);

        
        let splitViewController = self.window!.rootViewController as! UISplitViewController;
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController;
        navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem();
        splitViewController.delegate = self;
        let masterNavigationController = splitViewController.viewControllers[0] as! UINavigationController;
        let controller = masterNavigationController.topViewController as! MasterViewController;
        controller.managedObjectContext = self.managedObjectContext;
        return true;
    }

    func applicationWillResignActive(application: UIApplication)
    {
    }

    func applicationDidEnterBackground(application: UIApplication)
    {
    }

    func applicationWillEnterForeground(application: UIApplication)
    {
    }

    func applicationDidBecomeActive(application: UIApplication)
    {
    }

    func applicationWillTerminate(application: UIApplication)
    {
        self.saveContext();
    }

    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController!, ontoPrimaryViewController primaryViewController:UIViewController!) -> Bool
    {
        if let secondaryAsNavController = secondaryViewController as? UINavigationController
        {
            if let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController
            {
                if topAsDetailController.detailItem == nil
                {
                    return true;
                }
            }
        }
        return false;
    }

    lazy var applicationDocumentsDirectory: NSURL =
    {
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask);
        return urls[urls.count-1] as! NSURL;
    }()

    lazy var managedObjectModel: NSManagedObjectModel =
    {
        let modelURL = NSBundle.mainBundle().URLForResource("digital", withExtension: "momd")!;
        return NSManagedObjectModel(contentsOfURL: modelURL)!;
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? =
    {
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel);
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("digital.sqlite");
        var error: NSError? = nil;
        var failureReason = "There was an error creating or loading the application's saved data.";
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil
        {
            coordinator = nil;
            var dict = [String: AnyObject]();
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict);
            NSLog("Unresolved error \(error), \(error!.userInfo)");
            abort();
        }
        return coordinator;
    }()

    lazy var managedObjectContext: NSManagedObjectContext? =
    {
        let coordinator = self.persistentStoreCoordinator;
        if coordinator == nil
        {
            return nil;
        }
        var managedObjectContext = NSManagedObjectContext();
        managedObjectContext.persistentStoreCoordinator = coordinator;
        return managedObjectContext;
    }()

    func saveContext ()
    {
        if let moc = self.managedObjectContext
        {
            var error: NSError? = nil;
            if moc.hasChanges && !moc.save(&error)
            {
                NSLog("Unresolved error \(error), \(error!.userInfo)");
                abort();
            }
        }
    }
}