import UIKit
import CoreData

class MasterViewController: UITableViewController, NSFetchedResultsControllerDelegate, UISearchBarDelegate, UISearchDisplayDelegate
{
    @IBOutlet weak var searchBar:UISearchBar!;
    var load = true
    
    var artworkData:Array<Artwork> = [];
    var filteredArtworks = [Artwork]()
    
    func filterContentForSearchText(searchText: String) {
        // Filter the array using the filter method
        self.filteredArtworks = self.artworkData.filter({( artwork: Artwork) -> Bool in
            let titleMatch = artwork.artworkTitle.lowercaseString.rangeOfString(searchText.lowercaseString);
            let nationalityMatch = artwork.artistNationality.lowercaseString.rangeOfString(searchText.lowercaseString);
            let artistNameMatch = artwork.artistFirstName.lowercaseString.rangeOfString(searchText.lowercaseString);
            let artistNameMatchLast = artwork.artistLastName.lowercaseString.rangeOfString(searchText.lowercaseString);
            let mediumMatch = artwork.artworkMedium.lowercaseString.rangeOfString(searchText.lowercaseString);
            let yearMatch = String(artwork.artworkYear).lowercaseString.rangeOfString(searchText.lowercaseString);
            let exhibitionMatch = artwork.artworkExhibitionName.lowercaseString.rangeOfString(searchText.lowercaseString);
            let speciesMatch = artwork.artworkSpecies.lowercaseString.rangeOfString(searchText.lowercaseString)
            return (titleMatch != nil) || (nationalityMatch != nil) || (artistNameMatch != nil) || (artistNameMatchLast != nil) || (mediumMatch != nil) || (yearMatch != nil) || (exhibitionMatch != nil) || (speciesMatch != nil);
        })
    }

    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        return true
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if tableView as? UITableView == self.searchDisplayController!.searchResultsTableView {
            self.performSegueWithIdentifier("showDetail", sender: tableView);
        }
    }
    
    
    
    var detailViewController: DetailViewController? = nil;
    var managedObjectContext: NSManagedObjectContext? = nil;

    func getJsonData() -> Array<Artwork>
    {
        let path = NSBundle.mainBundle().pathForResource("BIA40", ofType: "json");
        let text = String(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)!;
        let data:NSData = text.dataUsingEncoding(NSUTF8StringEncoding)!;
        var error:NSError?;
        let anyObj:AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(0), error: &error);
        var list:Array<Artwork> = [];
        if anyObj is Array<AnyObject>
        {
            var a:Artwork = Artwork();
            for json in anyObj as! Array<AnyObject>
            {
                a.id = (json["BIA 40"] as AnyObject? as? Int) ?? 0;
                a.artworkExhibitionName = (json["Birds in Art anniversary"] as AnyObject? as? String) ?? "Unknown Exhibition";
                a.artworkTitle = (json["Title"] as AnyObject? as? String) ?? "Unknown Title";
                a.artworkYear = (json["Year"] as AnyObject? as? Int) ?? 2015;
                a.artworkMedium = (json["Medium"] as AnyObject? as? String) ?? "Unknown Medium";
                a.artistFirstName = (json["First Name"] as AnyObject? as? String) ?? String();
                a.artistLastName = (json["Last Name"] as AnyObject? as? String) ?? String();
                a.artistLifeDates = (json["Life Dates"] as AnyObject? as? String) ?? "Unknown Life Dates";
                a.artistNationality = (json["Nationality"] as AnyObject? as? String) ?? "Unknown Nationality";
                a.source = (json["Source of Acquisition"] as AnyObject? as? String) ?? "Unknown Source of Acquisition";
                a.artworkUnframedDimensions = (json["Unframed Dimensions"] as AnyObject? as? String) ?? "Unknown Dimensions";
                a.artworkSpecies = (json["Species"] as AnyObject? as? String) ?? "Unknown Species";
                a.artworkSupport = (json["Support"] as AnyObject? as? String) ?? "Unknown Support";
                list.append(a);
            }
        }
        return list;
    }
    
    
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    
    
    
    

    override  func awakeFromNib() {
        super.awakeFromNib();
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Search, target: self, action: "click")
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
            self.clearsSelectionOnViewWillAppear = false
            self.preferredContentSize = CGSize(width: 320.0, height: 600.0)
        }
    }

    func click()
    {
        self.searchDisplayController?.setActive(true, animated: true)
    }
    
    override func viewDidAppear(animated: Bool) {
        if (self.numberOfSectionsInTableView(self.tableView) > 0 && load) {
            var top = NSIndexPath(forRow: Foundation.NSNotFound, inSection: 0);
            self.tableView.scrollToRowAtIndexPath(top, atScrollPosition: UITableViewScrollPosition.Top, animated: true);
            load = false
        }
    }
    
    override  func viewDidLoad() {
        super.viewDidLoad()
        
        


        
        artworkData = getJsonData();
        
        
        
    }

    override  func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

     func insertNewObject(sender: AnyObject) {
    }

    // MARK: - Segues

    override  func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if(segue.identifier == "showDetail")
        {
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
            if (self.searchDisplayController?.searchResultsTableView == sender as? UITableView)
            {
                let indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForSelectedRow()!
                let object = filteredArtworks[indexPath.row];
                controller.detailItem = object;
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
            else
            {
                let indexPath = self.tableView.indexPathForSelectedRow()!
                let object = artworkData[indexPath.row];
                controller.detailItem = object;
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }

    override  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            return self.filteredArtworks.count
        } else {
            return self.artworkData.count
        }
    }

    override  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as! UITableViewCell
        
        var artwork : Artwork
        // Check to see whether the normal table or search results table is being displayed and set the Candy object from the appropriate array
        if tableView == self.searchDisplayController!.searchResultsTableView {
            artwork = filteredArtworks[indexPath.row]
        } else {
            artwork = artworkData[indexPath.row]
        }
        
        // Configure the cell
        

        cell.textLabel!.text = artwork.artworkTitle
        cell.textLabel!.font = UIFont(name: "Avenir-BookOblique", size: 17)!

        
        
        
        
        cell.detailTextLabel!.text = artwork.artistFirstName + " " + artwork.artistLastName
        
        cell.detailTextLabel!.font = UIFont(name: "Avenir-Book", size: 14)!
        
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        
        return cell
    }

    override  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false;
    }

    override  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let context = self.fetchedResultsController.managedObjectContext
            context.deleteObject(self.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject)
                
            var error: NSError? = nil
            if !context.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //println("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
        }
    }


    // MARK: - Fetched results controller

     var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("Event", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "timeStamp", ascending: false)
        let sortDescriptors = [sortDescriptor]
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
    	var error: NSError? = nil
    	if !_fetchedResultsController!.performFetch(&error) {
    	     // Replace this implementation with code to handle the error appropriately.
    	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
             //println("Unresolved error \(error), \(error.userInfo)")
    	     abort()
    	}
        
        return _fetchedResultsController!
    }    
     var _fetchedResultsController: NSFetchedResultsController? = nil

     func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }

     func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
            case .Insert:
                self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            case .Delete:
                self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
            default:
                return
        }
    }

     func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        return;
    }

     func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }

}

