import Foundation

public struct Artwork
{
    var id:Int = 0;
    var artworkExhibitionName:String = "Birds in Art";
    var artworkTitle:String = "N/A";
    var artworkYear:Int = 0;
    var artworkMedium:String = "";
    var artworkUnframedDimensions:String = "";
    var artworkSpecies:String = "";
    var artworkSupport:String = "";
    var artistFirstName:String = "John";
    var artistLastName:String = "Doe";
    var artistLifeDates:String = "0000-0000";
    var artistNationality:String = "N/A";
    var source:String = "";
}